﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Frontend
{
    public partial class MainWindow : Window
    {
        private readonly HttpClient client;

        public MainWindow()
        {
            client = new HttpClient();
        }

        public void Init()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Call the API with Configure command to load the JSON definitions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfig_Click(object sender, RoutedEventArgs e)
        {
            string unitdefinitions = GET(Server.Text + "ConfigureService/" + ConfigPath.Text);
            PopulateViewList(unitdefinitions);
        }

        /// <summary>
        /// Call the API with Conversion command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConvert_Click(object sender, RoutedEventArgs e)
        {
            string command = Server.Text + "Convert/" + Unit1.Text + "_" + Unit2.Text + "_" + Value.Text;
            Output.Content = GET(command);
        }

        /// <summary>
        /// Creates a list of available Units from API response
        /// </summary>
        /// <param name="unitdefinitions">API response string</param>
        private void PopulateViewList(string unitdefinitions)
        {
            List<string> Names, Symbols;
            Names = new List<string>();
            Symbols = new List<string>();

            string[] Quantities = unitdefinitions.Split(';');

            for (int i = 0; i < Quantities.Length-1; i++)
            {
                string[] Units = (Quantities[i].Split('_')[1]).Split(',');
                for (int j = 0; j < Units.Length; j++)
                {
                    ListBoxItem itm = new ListBoxItem();
                    itm.Content = Units[j];
                    UnitList.Items.Add(itm);
                }
            }
        }

        /// <summary>
        /// Simple GET Request creator
        /// </summary>
        /// <param name="URL">URL of the request</param>
        /// <returns>Response</returns>
        public string GET(string URL)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }
    }
}