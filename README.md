About:

Unit converter for Linear conversions - i.e. no Farenheit to Celsius. Units are defined in JSON, e.g. Weight_Kg.json includes Kilogram, Gram, Milligram, Pound and Ounce.

Project is divided into Backend and simple Frontend. Backend is a dotnet core REST API, frontend is a WPF App.

This allows the Backend to be containerized with Docker and orchestrated as a microservice. dotnet core was chosen as the platform for this reason for the Backend.

Backend contains an In Memory Database (Entity Framework). This can also be connected instead to other DBs, e.g MySQL containers, also orchestrated as Microservice with Persistent Volumes.



Usage:

The REST API launches at localhost:5000, but this can be configured by modifying 'Backend/Properties/launchsettings.json'

Compiled on windows 7 with Visual Studio 2017. 



With Frontend:

- git clone this repository

- Start frontend and backend. /Backend/Release x64/RestAPI.exe and /Frontend/Release x64/Frontend.exe

- In frontend, click Load Configuration. The list of Units should be loaded into the REST Backend, and show up in the frontend.

- Enter a Unit in the Boxes Unit1, Unit2. This is case sensitive. For example Kilogram and Pound. Enter a value to convert. 

- Click convert

- Converted value should be shown on the Frontend.

With only Backend:

- To use the Backend API, you need to be able to make REST GET calls. For example with the Insomnia tool on Windows or Curl on Linux.

- Restart the backend.

- Tell the backend to load configuration. The syntax is: http://localhost:5000/api/Unit/ConfigureService/{path to JSON files}

Using Insomnia, the call would be for example: GET http://localhost:5000/api/Unit/ConfigureService/Config where /Config is the default JSON location in the build.

- You should get a response that includes a definition of all Units. If you use GET http://localhost:5000/api/Unit/ConfigureService/Config , you will receive

"Energy(J_Kilojoule(KJ),Joule(J),Millijoule(mJ),Calorie(cal),Kilocalorie(Kcal),BritishThermalUnit(BTU);
Length(m_Kilometer(Km),Meter(m),Millimeter(mm),Centimeter(cm),Foot(ft),Inch(in),Yard(yd);
Weight(Kg_Kilogram(Kg),Gram(g),Milligram(mg),Pound(lb),Ounce(oz);"

- Call the Convert command. Syntax is: http://localhost:5000/api/Unit/Convert/{Unit1_Unit2_Value} , where Unit1 and Unit2 are case sensitive Unit names and Value is the value of Unit1 to convert to Unit2

For example, with Insomnia, the call could be GET http://localhost:5000/api/Unit/Convert/Kilogram_Pound_100 and the response would be 220.75055187637969





Docker

- Running the container with dockercompose:

docker-compose -f ./Docker/docker-compose.yml build --no-cache

docker-compose -f ./Docker/dockercompose.yml up -d


- Orchestration on Kubernetes (gke) [Currently INCOMPLETE and not working !]


Deployment and service configs are in .\k8

kubectl create -f rest-api.yaml

kubectl create -f rest-api-service.yaml