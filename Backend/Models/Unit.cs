using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ConversionService.Models
{

    /// <summary>
    /// Stores Quantity details -e.g. Weight, baseunit (Kg), Units of Quantity (Kilogram, Gram, Milligram, Pound, Ounce)
    /// Singleton Class
    /// </summary>
    public class Quantities
    {
        public static Quantities GetSingleton()
        {
            return _singleton;
        }

        private static readonly Quantities _singleton = new Quantities();

        private Quantities()
        {
            _IDs = new Dictionary<string, int>();
            _units = new Dictionary<string, List<int>>();
            _baseunits = new Dictionary<string, string>();
        }

        public Dictionary<string, int> IDs
        {
            get
            {
                return this._IDs;
            }
            set
            {
                this._IDs = value;
            }
        }

        public Dictionary<string, List<int>> Units
        {
            get
            {
                return this._units;
            }
            set
            {
                this._units = value;
            }
        }

        public Dictionary<string, string> Baseunits
        {
            get
            {
                return this._baseunits;
            }
            set
            {
                this._baseunits = value;
            }
        }

        private Dictionary<string, int> _IDs;
        private Dictionary<string, List<int>> _units;
        private Dictionary<string, string> _baseunits;
    }

    /// <summary>
    /// Unit class stored in the DB. ID is the primary key. e.g {0, Kilogram, Kg, Weight, No, 1000.000}
    /// </summary>
    public class Unit
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(8)]
        public string Symbol { get; set; }

        [Required]
        [StringLength(50)]
        public string Quantity { get; set; }

        [Required]
        public bool isSIUnit { get; set; }
 
        [Required]
        public double SIConversionFactor { get; set; }
    }
}