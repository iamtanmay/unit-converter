using Microsoft.EntityFrameworkCore;

namespace ConversionService.Models
{
    public class UnitContext: DbContext
    {
        public DbSet<Unit> Units { get; set; }

        public UnitContext(DbContextOptions options): base(options) { }
    }
}