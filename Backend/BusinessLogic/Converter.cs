﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConversionService.Controllers
{
    public static class Converter
    {
        /// <summary>
        /// Calculates Value interchange using the SI scale factors for both values.
        /// </summary>
        /// <param name="SIConversionFactor1">SI Conversion Factor of Unit 1</param>
        /// <param name="SIConversionFactor2">SI Conversion Factor of Unit 2</param>
        /// <param name="Value">Value of Unit 1 to convert to Unit 2</param>
        /// <returns></returns>
        public static double Convert(double SIConversionFactor1, double SIConversionFactor2, double Value)
        {
            double oValue = ((SIConversionFactor1 * Value) / SIConversionFactor2);
            return oValue;
        }
    }
}