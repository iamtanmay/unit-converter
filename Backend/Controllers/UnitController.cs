using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http;
using System.Collections.Generic;
using ConversionService.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ConversionService.Controllers
{
    [Route("/api/[controller]")]
    public class UnitController : InjectedController
    {
        public Quantities quantities;

        public UnitController(UnitContext context) : base(context)
        {
            quantities = Quantities.GetSingleton();
        }

        /// <summary>
        /// Conversion API
        /// </summary>
        /// <param name="query">[Unit1][Unit2][ValueToConvert] separated by _ delimiter</param>
        /// <returns>string of converted value</returns>
        [HttpGet("Convert/{query}", Name = "Convert")]
        public async Task<IActionResult> Convert(string query)
        {
            string[] args = query.Split('_');

            if (args.Length != 3)
                return Ok("Error - Badly formed request " + query);

            string unit1 = args[0];
            string unit2 = args[1];
            double val = 0f;
            double.TryParse(args[2], out val);

            if (!quantities.IDs.ContainsKey(unit1))
            {
                return Ok("Error - Unit undefined " + unit1);
            }

            int ID1 = quantities.IDs[unit1];
            Unit dbUnit1 = (await db.Units.FindAsync(ID1));

            if (!quantities.IDs.ContainsKey(unit2))
            {
                return Ok("Error - Unit undefined " + unit2);
            }

            int ID2 = quantities.IDs[unit2];
            Unit dbUnit2 = (await db.Units.FindAsync(ID2));

            //Check if both Units belong to same quantity
            if (dbUnit1.Quantity!=dbUnit2.Quantity)
                return Ok("Error - Units are of different Quantities " + dbUnit1.Quantity + " " + dbUnit2.Quantity);

            double result = Converter.Convert(dbUnit1.SIConversionFactor, dbUnit2.SIConversionFactor, val);

            return Ok(result);
        }

        /// <summary>
        /// Configure the converter by reading JSON files for Units
        /// </summary>
        /// <param name="query">Path to Config files</param>
        /// <returns>String containing every Unit and its Symbol. Quantities separated by ; ,
        /// units by ',' and Unit symbols by "()"
        /// '_' delimiter after Quantity name and Baseunit</returns>
        [HttpGet("ConfigureService/{query}", Name = "ConfigureService")]
        public async Task<IActionResult> ConfigureService(string query)
        {
            ReadConfig(query);
            string returnstring = "";

            foreach (KeyValuePair<string, List<int>> Quantity in quantities.Units)
            {
                //If Quantity has no units, do not return
                if (Quantity.Value.Count > 0)
                {
                    int[] UnitIDs = Quantity.Value.ToArray();
                    string[] Unitnames = new string[UnitIDs.Length];
                    string[] Unitsymbols = new string[UnitIDs.Length];

                    for (int i = 0; i < UnitIDs.Length; i++)
                    {
                        Unit unit = await db.Units.FindAsync(UnitIDs[i]);
                        Unitnames[i] = unit.Name;
                        Unitsymbols[i] = unit.Symbol;
                    }

                    returnstring += GetQuantityDetails(Quantity.Key, quantities.Baseunits[Quantity.Key], Unitnames, Unitsymbols) + ";";
                }
            }

            //Remove last ;
            if (returnstring.Length < 1)
                returnstring = returnstring.Remove(returnstring.Length - 1);

            return Ok(returnstring);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="QuantityName"></param>
        /// <param name="BaseUnit"></param>
        /// <param name="UnitNames"></param>
        /// <param name="UnitSymbols"></param>
        /// <returns></returns>
        private string GetQuantityDetails(string QuantityName, string BaseUnit, string[] UnitNames, string[] UnitSymbols)
        {
            string returnstring = QuantityName + "(" + BaseUnit + "_";

            for (int i=0; i< UnitNames.Length; i++)
            {
                returnstring += UnitNames[i] + "(" + UnitSymbols[i] + "),";
            }

            //Remove last comma
            returnstring = returnstring.Remove(returnstring.Length - 1);
            return returnstring;
        }

        /// <summary>
        /// Reads all JSON files at location
        /// </summary>
        /// <param name="Path">JSON file location</param>
        /// <returns>Number of Units loaded</returns>
        private int ReadConfig(string Path)
        {
            string path = Path;

            try
            {
                DirectoryInfo di = new DirectoryInfo(path);

                foreach (FileInfo file in di.GetFiles("*.json", SearchOption.TopDirectoryOnly))
                {
                    string json = System.IO.File.ReadAllText(file.FullName);

                    string filename = System.IO.Path.GetFileNameWithoutExtension(file.FullName);
                    string baseunit = filename.Split('_')[1];
                    string quantityname = filename.Split('_')[0];

                    AddQuantity(json, quantityname, baseunit);
                }
            }
            catch
            {
                return 0;
            }
            return quantities.IDs.Count;
        }

        /// <summary>
        /// Adds a Quantity from a JSON file
        /// </summary>
        /// <param name="JSON">JSON file</param>
        /// <param name="QuantityName">Quantity name</param>
        /// <param name="BaseUnit">Quantity baseunit e.g. Kg for Weight, m for Length</param>
        private void AddQuantity(string JSON, string QuantityName, string BaseUnit)
        {
            List<Unit> units = new List<Unit>();
            units = JsonConvert.DeserializeObject<List<Unit>>(JSON);

            quantities.Units.Add(QuantityName, new List<int>());
            quantities.Baseunits.Add(QuantityName, BaseUnit);
            foreach (Unit unit in units)
            {
                unit.Quantity = QuantityName;
                int ID = AddUnit(unit);
                quantities.IDs.Add(unit.Name, ID);
                quantities.Units[QuantityName].Add(ID);
            }
        }

        /// <summary>
        /// Add Unit to DB
        /// </summary>
        /// <param name="unit"></param>
        /// <returns>ID (Primary Key) of Unit in database</returns>
        private int AddUnit(Unit unit)
        {
            db.Units.Add(unit);
            db.SaveChanges();
            return unit.ID;
        }
    }
}