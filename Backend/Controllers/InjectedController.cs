using ConversionService.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ConversionService.Controllers
{
    public class InjectedController: ControllerBase
    {
        protected readonly UnitContext db;

        public InjectedController(UnitContext context)
        {
            db = context;
        }
        
    }
}